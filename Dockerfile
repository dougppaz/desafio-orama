FROM python:3.8.5-alpine

WORKDIR /home/app

RUN pip install gunicorn

COPY requirements.txt requirements.txt

RUN pip install -r requirements.txt

COPY freelance_months freelance_months
COPY wsgi.py wsgi.py

ENTRYPOINT [ "gunicorn", "-b 0.0.0.0:5000", "--worker-connections=1024", "wsgi:wsgi_app" ]
