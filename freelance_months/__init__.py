"""
Calculate Freelance Skills Duration in Months by Douglas Paz
"""

from functools import reduce
from datetime import (
    datetime,
    timedelta
)


def parse(freelancer: dict) -> dict:
    """
    Parse a Freelancer Data

    Parameters
    ----------
    freelancer : dict
        Freelancer data with id and professional experiences.

    Raises
    ------
    AssertionError
        If has empty or invalid input
    ValueError
        If has invalid format value
    """
    freelance = freelancer.get('freelance')
    assert freelance is not None, \
        'freelance data not found'
    assert isinstance(freelance, dict), \
        'freelance data is not a dict'
    freelance_id = freelance.get('id')
    assert freelance_id is not None, \
        'freelance id not found'
    freelance_experiences = freelance.get('professionalExperiences')
    assert freelance_experiences is not None, \
        'freelance experiences not found'
    assert isinstance(freelance_experiences, list), \
        'freelance experiences is not a list'
    return {
        'freelance': {
            'id': freelance_id,
            'computedSkills': _compute_skills(freelance_experiences)
        }
    }


def _compute_skills(experiences: list) -> list:
    return list(
        map(
            _compute_skill,
            reduce(_reduce_experience, experiences, dict()).values()
        )
    )


def _compute_skill(skill_months_tuple: tuple) -> dict:
    skill, months = skill_months_tuple
    return {
        'id': skill.get('id'),
        'name': skill.get('name'),
        'durationInMonths': len(months)
    }


def _reduce_experience(acc: dict, experience: dict) -> dict:
    start_date = experience.get('startDate')
    end_date = experience.get('endDate')
    months = _interval_months_list(start_date, end_date)
    skills = experience.get('skills')
    return reduce(
        lambda acc, skill: _reduce_skill(acc, skill, months),
        skills,
        acc
    )


def _reduce_skill(acc: dict, skill: dict, months: list) -> dict:
    skill_id = skill.get('id')
    assert skill_id is not None, \
        'skill id not found'
    if skill_id in acc:
        _, acc_months = acc.get(skill_id)
        months = _sorted_distinct_list(acc_months, months)
    acc.update({
        skill_id: (skill, months,)
    })
    return acc


def _interval_months_list(start_date: str, end_date: str) -> list:
    start = datetime.fromisoformat(start_date)
    end = datetime.fromisoformat(end_date)
    assert start < end, \
        f'the start date {start} is the after end date {end}'
    delta = end - start
    return _sorted_distinct_list(
        map(
            lambda day_n: (start + timedelta(days=day_n)).strftime(r'%Y-%m'),
            range(delta.days)
        )
    )


def _sorted_distinct_list(*args) -> list:
    return sorted(list(set(reduce(lambda acc, item: acc + item, args))))
