# Development

## Install dependencies

Using pipenv:

```bash
$ pipenv install --dev
```

Using pip:

```bash
$ pip install -r requirements.txt
$ pip install -r requirements-dev.txt
```

## Run API

Run API in development mode:

```bash
$ pipenv run api
```

## Update requirements.txt and requirements-dev.txt with pipenv

Update requirements.txt using pipenv

```bash
$ pipenv lock -r > requirements.txt
```

Update requirements-dev.txt using pipenv

```bash
$ pipenv lock -r --dev-only > requirements-dev.txt
```

## Lint

```bash
$ pipenv run lint
```

## Test

Run tests using pipenv script

```bash
$ pipenv run test
```

Run tests

```bash
$ python -m unittest discover freelance_months
```

## Coverage

Using pipenv script

```bash
$ pipenv run cover
$ pipenv run cover-report
```

Using coverage

```bash
$ coverage run
$ coverage report
```

## Docker

Build Docker image:

```bash
docker build -t freelance_months:latest .
```

Run Docker container:

```bash
docker run -it -p 5000:5000 freelance_months:latest
```
