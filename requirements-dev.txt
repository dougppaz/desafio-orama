-i https://pypi.org/simple
coverage==5.2.1
flake8==3.8.3
mccabe==0.6.1
pycodestyle==2.6.0; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3'
pyflakes==2.2.0; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3'
