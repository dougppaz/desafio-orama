import freelance_months
from flask import (
    Flask,
    request
)


app = Flask(__name__)


@app.route('/ping/')
def ping():
    return 'OK'


@app.route('/parse/', methods=['POST'])
def parse():
    content = request.json
    try:
        return freelance_months.parse(content)
    except AssertionError as e:
        return {'type': 'assertion_error', 'details': e.args[0]}, 422
    except ValueError as e:
        return {'type': 'value_error', 'details': e.args[0]}, 422
