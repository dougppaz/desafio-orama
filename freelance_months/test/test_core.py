import unittest
import freelance_months
from unittest.mock import patch


class ParseTest(unittest.TestCase):
    def test_empty_freelance(self):
        with self.assertRaisesRegex(AssertionError,
                                    'freelance data not found'):
            freelance_months.parse({})

    def test_invalid_freelance(self):
        with self.assertRaisesRegex(AssertionError,
                                    'freelance data is not a dict'):
            freelance_months.parse({'freelance': 0})

    def test_empty_freelance_id(self):
        with self.assertRaisesRegex(AssertionError,
                                    'freelance id not found'):
            freelance_months.parse({'freelance': {}})

    def test_empty_freelance_experiences(self):
        with self.assertRaisesRegex(AssertionError,
                                    'freelance experiences not found'):
            freelance_months.parse(
                {
                    'freelance': {
                        'id': 1
                    }
                }
            )

    def test_invalid_freelance_experiences(self):
        with self.assertRaisesRegex(AssertionError,
                                    'freelance experiences is not a list'):
            freelance_months.parse(
                {
                    'freelance': {
                        'id': 1,
                        'professionalExperiences': 0
                    }
                }
            )

    @patch('freelance_months._compute_skills', return_value=[])
    def test_valid(self, *args):
        self.assertDictEqual(
            freelance_months.parse(
                {
                    'freelance': {
                        'id': 1,
                        'professionalExperiences': []
                    }
                }
            ),
            {
                'freelance': {
                    'id': 1,
                    'computedSkills': []
                }
            }
        )


class ComputeSkillsTest(unittest.TestCase):
    def test_valid(self):
        self.assertListEqual(
            freelance_months._compute_skills(
                [
                    {
                        'id': 4,
                        'companyName': 'Okuneva, Kerluke and Strosin',
                        'startDate': '2016-01-01T00:00:00+01:00',
                        'endDate': '2018-05-01T00:00:00+01:00',
                        'skills': [
                            {
                                'id': 241,
                                'name': 'React'
                            },
                            {
                                'id': 270,
                                'name': 'Node.js'
                            },
                            {
                                'id': 370,
                                'name': 'Javascript'
                            }
                        ]
                    }
                ]
            ),
            [
                {
                    'id': 241,
                    'name': 'React',
                    'durationInMonths': 28
                },
                {
                    'id': 270,
                    'name': 'Node.js',
                    'durationInMonths': 28
                },
                {
                    'id': 370,
                    'name': 'Javascript',
                    'durationInMonths': 28
                }
            ]
        )


class ReduceExperienceTest(unittest.TestCase):
    def test_valid(self):
        self.assertDictEqual(
            freelance_months._reduce_experience(
                {
                    101: (
                        {
                            'id': 101,
                            'name': 'Node.js'
                        },
                        [
                            '2019-10',
                            '2019-12',
                            '2020-01'
                        ],
                    )
                },
                {
                    'id': 1,
                    'companyName': 'Okuneva, Kerluke and Strosin',
                    'startDate': '2020-01-01T00:00:00+01:00',
                    'endDate': '2020-05-01T00:00:00+01:00',
                    'skills': [
                        {
                            'id': 100,
                            'name': 'React'
                        },
                        {
                            'id': 101,
                            'name': 'Node.js'
                        }
                    ]
                }
            ),
            {
                100: (
                    {
                        'id': 100,
                        'name': 'React',
                    },
                    [
                        '2020-01',
                        '2020-02',
                        '2020-03',
                        '2020-04'
                    ],
                ),
                101: (
                    {
                        'id': 101,
                        'name': 'Node.js'
                    },
                    [
                        '2019-10',
                        '2019-12',
                        '2020-01',
                        '2020-02',
                        '2020-03',
                        '2020-04'
                    ],
                )
            }
        )


class ReduceSkillTest(unittest.TestCase):
    def test_empty_skill_id(self):
        with self.assertRaisesRegex(AssertionError,
                                    'skill id not found'):
            freelance_months._reduce_skill(
                {},
                {},
                []
            )

    def test_valid(self):
        self.assertDictEqual(
            freelance_months._reduce_skill(
                {},
                {
                    'id': 100,
                    'name': 'React'
                },
                ['2020-01', '2020-02']
            ),
            {
                100: (
                    {
                        'id': 100,
                        'name': 'React'
                    },
                    ['2020-01', '2020-02']
                )
            }
        )
        self.assertDictEqual(
            freelance_months._reduce_skill(
                {
                    100: ({}, ['2019-11'],)
                },
                {
                    'id': 100,
                    'name': 'React'
                },
                ['2020-01', '2020-02']
            ),
            {
                100: (
                    {
                        'id': 100,
                        'name': 'React'
                    },
                    ['2019-11', '2020-01', '2020-02']
                )
            }
        )


class GetMonthsListTest(unittest.TestCase):
    def test_invalid_date_format(self):
        with self.assertRaisesRegex(ValueError,
                                    r'Invalid isoformat string: \S+'):
            freelance_months._interval_months_list(
                '06/08/2020',
                '2020-09-01'
            )

    def test_start_date_after_end_date(self):
        with self.assertRaisesRegex(AssertionError,
                                    r'the start date \S+ \S+ is the after end '
                                    r'date \S+ \S+'):
            freelance_months._interval_months_list(
                '2020-10-01',
                '2020-09-01'
            )

    def test_valid(self):
        months_list = freelance_months._interval_months_list(
            '2020-06-01T00:00:00+01:00',
            '2020-09-01T00:00:00+01:00'
        )
        months_list.sort()
        self.assertListEqual(
            months_list,
            [
                '2020-06',
                '2020-07',
                '2020-08'
            ]
        )
        months_list = freelance_months._interval_months_list(
            '2019-11-01T00:00:00+01:00',
            '2020-02-01T00:00:00+01:00'
        )
        months_list.sort()
        self.assertListEqual(
            months_list,
            [
                '2019-11',
                '2019-12',
                '2020-01'
            ]
        )
        months_list = freelance_months._interval_months_list(
            '2019-11-01',
            '2020-02-01'
        )
        months_list.sort()
        self.assertListEqual(
            months_list,
            [
                '2019-11',
                '2019-12',
                '2020-01'
            ]
        )


class SortedDistinctListTest(unittest.TestCase):
    def test_valid(self):
        self.assertListEqual(
            freelance_months._sorted_distinct_list([0]),
            [0]
        )
        self.assertListEqual(
            freelance_months._sorted_distinct_list([0], [1]),
            [0, 1]
        )
        self.assertListEqual(
            freelance_months._sorted_distinct_list([1], [0]),
            [0, 1]
        )
