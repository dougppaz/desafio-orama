import unittest
from freelance_months.api import app


class PingRouteTest(unittest.TestCase):
    def setUp(self):
        client = app.test_client()
        self.response = client.get('/ping/')

    def test_status_code(self):
        self.assertEqual(200, self.response.status_code)

    def test_response_body(self):
        self.assertEqual('OK', self.response.data.decode('utf-8'))


class ParseRouteTest(unittest.TestCase):
    def setUp(self):
        self.client = app.test_client()

    def test_empty_freelance_id(self):
        response = self.client.post(
            '/parse/',
            json={
                'freelance': {}
            }
        )
        self.assertEqual(response.status_code, 422)
        self.assertDictEqual(
            response.json,
            {
                'type': 'assertion_error',
                'details': 'freelance id not found'
            }
        )

    def test_invalid_date(self):
        response = self.client.post(
            '/parse/',
            json={
                'freelance': {
                    'id': 1,
                    'professionalExperiences': [
                        {
                            'id': 1,
                            'startDate': 'invalid',
                            'endDate': 'invalid',
                            'skills': [
                                {
                                    'id': 100,
                                    'name': 'React'
                                }
                            ]
                        }
                    ]
                }
            }
        )
        self.assertEqual(response.status_code, 422)
        self.assertDictEqual(
            response.json,
            {
                'type': 'value_error',
                'details': 'Invalid isoformat string: \'invalid\''
            }
        )

    def test_valid(self):
        response = self.client.post(
            '/parse/',
            json={
                'freelance': {
                    'id': 1,
                    'professionalExperiences': [
                        {
                            'id': 1,
                            'startDate': '2020-05-01',
                            'endDate': '2020-07-01',
                            'skills': [
                                {
                                    'id': 100,
                                    'name': 'React'
                                }
                            ]
                        }
                    ]
                }
            }
        )
        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(
            response.json,
            {
                'freelance': {
                    'id': 1,
                    'computedSkills': [
                        {
                            'id': 100,
                            'name': 'React',
                            'durationInMonths': 2
                        }
                    ]
                }
            }
        )
